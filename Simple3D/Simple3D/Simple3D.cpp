﻿#include <fstream>
#include <strstream>
#include <algorithm>
#include <SFML/Graphics.hpp>
using namespace std;
/*
* Используемые ресурсы:
* https://www.sfml-dev.org/tutorials/2.5/start-vc.php - Присоединение библиотеки
* https://www.sfml-dev.org/documentation/2.5.1/annotated.php
*/

#define WIDTH 500.0f;
#define HEIGHT 500.0f;

struct mat4x4 {
	float m[4][4] = { 0 };
};

void DrawTriangle(int x1, int y1, int x2, int y2, int x3, int y3, sf::RenderWindow &window) {
	sf::VertexArray line1(sf::LinesStrip, 2);
	line1[0].position = sf::Vector2f(x1, y1);
	line1[0].color = sf::Color::White;
	line1[1].position = sf::Vector2f(x2, y2);
	line1[1].color = sf::Color::White;

	sf::VertexArray line2(sf::LinesStrip, 2);
	line2[0].position = sf::Vector2f(x2, y2);
	line2[0].color = sf::Color::White;
	line2[1].position = sf::Vector2f(x3, y3);
	line2[1].color = sf::Color::White;

	sf::VertexArray line3(sf::LinesStrip, 2);
	line3[0].position = sf::Vector2f(x3, y3);
	line3[0].color = sf::Color::White;
	line3[1].position = sf::Vector2f(x1, y1);
	line3[1].color = sf::Color::White;

	window.draw(line1);
	window.draw(line2);
	window.draw(line3);
}

void FillTriangle(int x1, int y1, int x2, int y2, int x3, int y3, sf::Color color, sf::RenderWindow &window) {
	sf::ConvexShape convex;
	convex.setPointCount(3);
	convex.setPoint(0, sf::Vector2f(x1, y1));
	convex.setPoint(1, sf::Vector2f(x2, y2));
	convex.setPoint(2, sf::Vector2f(x3, y3));
	convex.setFillColor(color);

	window.draw(convex);
}

struct vec3d{
	float x, y, z;
};

struct triangle {
	vec3d point[3]; 

	sf::Color color;
};

struct mesh {
	vector<triangle> triangles;

	bool LoadFromObjectFile(string sFilename){
		ifstream f(sFilename);
		if (!f.is_open())
			return false;

		// Локальная переменная, для сохранения вершин
		vector<vec3d> verts;

		while (!f.eof())
		{
			char line[128];
			f.getline(line, 128);

			strstream s;
			s << line;

			char junk;

			// v - vertex(вершина)
			if (line[0] == 'v')
			{
				vec3d v;
				s >> junk >> v.x >> v.y >> v.z;
				verts.push_back(v);
			}
			// f - face
			if (line[0] == 'f')
			{
				int f[3];
				s >> junk >> f[0] >> f[1] >> f[2];
				triangles.push_back({ verts[f[0] - 1], verts[f[1] - 1], verts[f[2] - 1] });
			}
		}

		return true;
	}

};

class Engine3D {
public:
	Engine3D() {

	}
private:
	const float width = WIDTH;
	const float height = HEIGHT;
	mesh meshCube;
	mat4x4 matProj;

	float fTheta;

	void MultiplyMatrixVector(vec3d &in, vec3d &out, mat4x4 &m) {
		out.x = in.x * m.m[0][0] + in.y * m.m[1][0] + in.z * m.m[2][0] + m.m[3][0];
		out.y = in.x * m.m[0][1] + in.y * m.m[1][1] + in.z * m.m[2][1] + m.m[3][1];
		out.z = in.x * m.m[0][2] + in.y * m.m[1][2] + in.z * m.m[2][2] + m.m[3][2];
		float w = in.x * m.m[0][3] + in.y * m.m[1][3] + in.z * m.m[2][3] + m.m[3][3];

		if (w != 0.0f){
			out.x /= w; 
			out.y /= w; 
			out.z /= w;
		}
	}

	mat4x4 Matrix_MakeIdentity(){
		mat4x4 matrix;
		matrix.m[0][0] = 1.0f;
		matrix.m[1][1] = 1.0f;
		matrix.m[2][2] = 1.0f;
		matrix.m[3][3] = 1.0f;
		return matrix;
	}

	mat4x4 Matrix_MakeRotationX(float fAngleRad){
		mat4x4 matrix;
		matrix.m[0][0] = 1.0f;
		matrix.m[1][1] = cosf(fAngleRad);
		matrix.m[1][2] = sinf(fAngleRad);
		matrix.m[2][1] = -sinf(fAngleRad);
		matrix.m[2][2] = cosf(fAngleRad);
		matrix.m[3][3] = 1.0f;
		return matrix;
	}

	mat4x4 Matrix_MakeRotationY(float fAngleRad){
		mat4x4 matrix;
		matrix.m[0][0] = cosf(fAngleRad);
		matrix.m[0][2] = sinf(fAngleRad);
		matrix.m[2][0] = -sinf(fAngleRad);
		matrix.m[1][1] = 1.0f;
		matrix.m[2][2] = cosf(fAngleRad);
		matrix.m[3][3] = 1.0f;
		return matrix;
	}

	mat4x4 Matrix_MakeRotationZ(float fAngleRad){
		mat4x4 matrix;
		matrix.m[0][0] = cosf(fAngleRad);
		matrix.m[0][1] = sinf(fAngleRad);
		matrix.m[1][0] = -sinf(fAngleRad);
		matrix.m[1][1] = cosf(fAngleRad);
		matrix.m[2][2] = 1.0f;
		matrix.m[3][3] = 1.0f;
		return matrix;
	}

	mat4x4 Matrix_MakeProjection(float fFovDegrees, float fAspectRatio, float fNear, float fFar){
		float fFovRad = 1.0f / tanf(fFovDegrees * 0.5f / 180.0f * 3.14159f);
		mat4x4 matrix;
		matrix.m[0][0] = fAspectRatio * fFovRad;
		matrix.m[1][1] = fFovRad;
		matrix.m[2][2] = fFar / (fFar - fNear);
		matrix.m[3][2] = (-fFar * fNear) / (fFar - fNear);
		matrix.m[2][3] = 1.0f;
		matrix.m[3][3] = 0.0f;
		return matrix;
	}


public:
	bool onUserCreate() {
		/*
		meshCube.triangles = {
			
			// Задаем модель пирамиды 
			{ 0.0f, 0.0f, 0.0f,    1.0f, 0.0f, 0.0f,    0.0f, 0.0f, 1.0f },
			{ 0.0f, 0.0f, 1.0f,    1.0f, 0.0f, 0.0f,    1.0f, 0.0f, 1.0f },
                                               
			{ 0.0f, 0.0f, 0.0f,    0.5f, 1.0f, 0.5f,    0.5f, 0.0f, 0.0f },
			{ 0.5f, 0.0f, 0.0f,    0.5f, 1.0f, 0.5f,    1.0f, 0.0f, 0.0f },
                                                  
			{ 1.0f, 0.0f, 0.0f,    0.5f, 1.0f, 0.5f,    1.0f, 0.0f, 0.5f },
			{ 1.0f, 0.0f, 0.5f,    0.5f, 1.0f, 0.5f,    1.0f, 0.0f, 1.0f },
                                                  
			{ 1.0f, 0.0f, 1.0f,    0.5f, 1.0f, 0.5f,    0.5f, 0.0f, 1.0f },
			{ 0.5f, 0.0f, 1.0f,    0.5f, 1.0f, 0.5f,    0.0f, 0.0f, 1.0f },
                                                    
			{ 0.0f, 0.0f, 1.0f,    0.5f, 1.0f, 0.5f,    0.0f, 0.0f, 0.5f },
			{ 0.0f, 0.0f, 0.5f,    0.5f, 1.0f, 0.5f,    0.0f, 0.0f, 0.0f },
			
			
			// Задаем модель куба
			{ 0.0f, 0.0f, 0.0f,    0.0f, 1.0f, 0.0f,    1.0f, 1.0f, 0.0f },
			{ 0.0f, 0.0f, 0.0f,    1.0f, 1.0f, 0.0f,    1.0f, 0.0f, 0.0f },
                                                  
			{ 1.0f, 0.0f, 0.0f,    1.0f, 1.0f, 0.0f,    1.0f, 1.0f, 1.0f },
			{ 1.0f, 0.0f, 0.0f,    1.0f, 1.0f, 1.0f,    1.0f, 0.0f, 1.0f },
                                                  
			{ 1.0f, 0.0f, 1.0f,    1.0f, 1.0f, 1.0f,    0.0f, 1.0f, 1.0f },
			{ 1.0f, 0.0f, 1.0f,    0.0f, 1.0f, 1.0f,    0.0f, 0.0f, 1.0f },
                                                    
			{ 0.0f, 0.0f, 1.0f,    0.0f, 1.0f, 1.0f,    0.0f, 1.0f, 0.0f },
			{ 0.0f, 0.0f, 1.0f,    0.0f, 1.0f, 0.0f,    0.0f, 0.0f, 0.0f },
                                                    
			{ 0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 1.0f,    1.0f, 1.0f, 1.0f },
			{ 0.0f, 1.0f, 0.0f,    1.0f, 1.0f, 1.0f,    1.0f, 1.0f, 0.0f },
                                                   
			{ 1.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 0.0f },
			{ 1.0f, 0.0f, 1.0f,    0.0f, 0.0f, 0.0f,    1.0f, 0.0f, 0.0f },
			
		};
		*/

		// Загружаем модель объекта из файла
		meshCube.LoadFromObjectFile("teapot.obj");

		matProj = Matrix_MakeProjection(90.0f, height / width, 0.1f, 1000.0f);

		return true;
	}

	bool onUserUpdate(float fElapsedTime, sf::RenderWindow &window) {
		window.clear();

		mat4x4 matRotZ, matRotX;
		fTheta += 1.f * fElapsedTime;

		matRotZ = Matrix_MakeRotationZ(fTheta * 0.0001f);
		matRotX = Matrix_MakeRotationX(fTheta * 0.005f);

		// Сохраняем треугольники, чтобы позже их растеризовать
		vector<triangle> vecTrianglesToRaster;

		for (auto tri : meshCube.triangles) {
			triangle triProjected, triTranslated, triRotatedZ, triRotatedZX;

			// Поворот в оси Z
			MultiplyMatrixVector(tri.point[0], triRotatedZ.point[0], matRotZ);
			MultiplyMatrixVector(tri.point[1], triRotatedZ.point[1], matRotZ);
			MultiplyMatrixVector(tri.point[2], triRotatedZ.point[2], matRotZ);

			// Поворот в оси X
			MultiplyMatrixVector(triRotatedZ.point[0], triRotatedZX.point[0], matRotX);
			MultiplyMatrixVector(triRotatedZ.point[1], triRotatedZX.point[1], matRotX);
			MultiplyMatrixVector(triRotatedZ.point[2], triRotatedZX.point[2], matRotX);

			// Отступ от экрана
			triTranslated = triRotatedZX;
			triTranslated.point[0].z = triRotatedZX.point[0].z + 8.0f;
			triTranslated.point[1].z = triRotatedZX.point[1].z + 8.0f;
			triTranslated.point[2].z = triRotatedZX.point[2].z + 8.0f;

			// Используем векторное произведение, чтобы получить нормаль поверхности
			vec3d normal, line1, line2;
			line1.x = triTranslated.point[1].x - triTranslated.point[0].x;
			line1.y = triTranslated.point[1].y - triTranslated.point[0].y;
			line1.z = triTranslated.point[1].z - triTranslated.point[0].z;

			line2.x = triTranslated.point[2].x - triTranslated.point[0].x;
			line2.y = triTranslated.point[2].y - triTranslated.point[0].y;
			line2.z = triTranslated.point[2].z - triTranslated.point[0].z;

			normal.x = line1.y * line2.z - line1.z * line2.y;
			normal.y = line1.z * line2.x - line1.x * line2.z;
			normal.z = line1.x * line2.y - line1.y * line2.x;

			float l = sqrtf(normal.x*normal.x + normal.y*normal.y + normal.z*normal.z);
			normal.x /= l; normal.y /= l; normal.z /= l;

			if (normal.x * triTranslated.point[0].x +
				normal.y * triTranslated.point[0].y +
				normal.z * triTranslated.point[0].z < 0.0f) {

				// Добавляем свет
				vec3d light_direction = { 0.0f, 0.0f, -1.0f }; // Вектор света (напрвален на зрителя)
				float l = sqrtf(light_direction.x*light_direction.x + light_direction.y*light_direction.y + light_direction.z*light_direction.z);
				light_direction.x /= l; light_direction.y /= l; light_direction.z /= l;

				// Проверяем как сильно близка нормаль к направлению света
				float dp = normal.x * light_direction.x + normal.y * light_direction.y + normal.z * light_direction.z;

				// Выбираем цвет с коэфициентом наклона к источнику света
				sf::Color color(255 * dp, 255 * dp, 255 * dp);
				triTranslated.color = color;

				// Проецирование треугольников из 3D в 2D
				MultiplyMatrixVector(triTranslated.point[0], triProjected.point[0], matProj);
				MultiplyMatrixVector(triTranslated.point[1], triProjected.point[1], matProj);
				MultiplyMatrixVector(triTranslated.point[2], triProjected.point[2], matProj);
				triProjected.color = color;

				// Масштабирование на экране
				triProjected.point[0].x += 1.0f; triProjected.point[0].y += 1.0f;
				triProjected.point[1].x += 1.0f; triProjected.point[1].y += 1.0f;
				triProjected.point[2].x += 1.0f; triProjected.point[2].y += 1.0f;
				triProjected.point[0].x *= 0.5f * (float)width;
				triProjected.point[0].y *= 0.5f * (float)height;
				triProjected.point[1].x *= 0.5f * (float)width;
				triProjected.point[1].y *= 0.5f * (float)height;
				triProjected.point[2].x *= 0.5f * (float)width;
				triProjected.point[2].y *= 0.5f * (float)height;

				// Т.к. некотрые части прорисовываются поверх тех, что ближе к нам, надо использовать алгоритм художника 

				// Сохраняем треугольники для сортировки
				vecTrianglesToRaster.push_back(triProjected);
			}

		}

		// Сортируем треугольники от самых дальних, до ближних
		sort(vecTrianglesToRaster.begin(), vecTrianglesToRaster.end(), [](triangle &t1, triangle &t2){
			float z1 = (t1.point[0].z + t1.point[1].z + t1.point[2].z) / 3.0f;
			float z2 = (t2.point[0].z + t2.point[1].z + t2.point[2].z) / 3.0f;
			return z1 > z2;
		});

		for (auto &triProjected : vecTrianglesToRaster){
			// Растеризуем треугольник
			FillTriangle(triProjected.point[0].x, triProjected.point[0].y,
				triProjected.point[1].x, triProjected.point[1].y,
				triProjected.point[2].x, triProjected.point[2].y, triProjected.color, window);

		}

		return true;
	}

};

int main(){
	sf::RenderWindow window(sf::VideoMode(500, 500), "Simple3D");

	// Рекомендуется изменять количество кадров в секунду(FPS), если кадры пропадают
	window.setFramerateLimit(30); // 30 FPS
	Engine3D engine;
	engine.onUserCreate();
	sf::Event event;
	
	float fElapsedTime = 5.0f;
	
	while (window.isOpen()){
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		engine.onUserUpdate(fElapsedTime, window);
		window.display();

	}

	return 0;
}